package com.macrodroid;

import com.macrodroid.pages.DemoPage;
import com.macrodroid.pages.HomePage;
import com.macrodroid.pages.elements.MacrosCategory;
import com.macrodroid.pages.mainmenu.MacrosListPage;
import com.macrodroid.pages.mainmenu.addmacro.ActionsPage;
import com.macrodroid.pages.mainmenu.addmacro.ConstraintsPage;
import com.macrodroid.pages.mainmenu.addmacro.SaveMacroDialog;
import com.macrodroid.pages.mainmenu.addmacro.TriggersPage;
import com.macrodroid.pages.mainmenu.addmacro.actions.alarmclock.AlarmClock;
import com.macrodroid.pages.mainmenu.addmacro.triggers.batterylevel.BatteryLevelTriggerSettings;
import com.macrodroid.pages.mainmenu.addmacro.triggers.batterylevel.BattetyLevel;
import com.qatestlab.base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by aptashnik on 12/14/2016.
 */
@Test(priority = 1)
public class AddMacroTestCase extends BaseTest {
    @Test
    public void test() {
        HomePage homePage = new DemoPage().get().skip().get();
        TriggersPage triggersPage = homePage.nextPage(TriggersPage.class).get();
        triggersPage.selectTrigger(BattetyLevel.class).get()
                .fireBatteryLevelTrigger(BattetyLevel.WhenBatteryLevel.INCREASES_OR_DECREASES).accept();
        BatteryLevelTriggerSettings batteryLevelTriggerSettings = new BatteryLevelTriggerSettings().get().setSeekBarPercent(30);
        batteryLevelTriggerSettings.accept();

        ActionsPage actionsPage = new ActionsPage().get();
        actionsPage.nextPage(AlarmClock.class).get()
                .disableAlarmWhenTriggerFired().setAlarmClockLabel("iojhnvcxkzhkj").accept();
        actionsPage.get();
        ConstraintsPage constraintsPage = actionsPage.confirm().get();
        String macrosName = "the first macro created automatically";
        SaveMacroDialog saveMacroDialog = constraintsPage.confirm().get();
        saveMacroDialog.setMacroName(macrosName)
                .setMacroCategory("notifications")
                .accept();
        homePage.get();
        MacrosListPage macrosListPage = homePage.nextPage(MacrosListPage.class).get();
        MacrosCategory macrosCategory = macrosListPage.get(0);
        Assert.assertTrue(macrosCategory.contains(macros -> macros.getName().equals(macrosName)));
    }
}
