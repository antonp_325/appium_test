package com.macrodroid;

import com.macrodroid.pages.DemoPage;
import com.macrodroid.pages.HomePage;
import com.macrodroid.pages.mainmenu.SettingsPage;
import com.macrodroid.pages.mainmenu.settings.EditCategoriesPage;
import com.macrodroid.pages.mainmenu.settings.editcategories.EditCategoryNameDialog;
import com.qatestlab.base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by aptashnik on 12/19/2016.
 */
public class AddNewMacrosCategoryTestCase extends BaseTest {
    @Test
    public void test() {
        HomePage homePage = new DemoPage().get().skip().get();
        SettingsPage settingsPage = homePage.nextPage(SettingsPage.class).get();
        EditCategoriesPage editCategoriesPage = settingsPage.nextPage(EditCategoriesPage.class).get();
        EditCategoryNameDialog editCategoryNameDialog = editCategoriesPage.newCategory().get();

        String newCategoryName = "hight performance";

        editCategoryNameDialog.setCategoryName(newCategoryName).accept();
        editCategoriesPage.get();

        Assert.assertTrue(editCategoriesPage.contains(category -> category.getTitle().equals(newCategoryName)));
    }
}
