package com.macrodroid.pages.elements;

import com.macrodroid.pages.elements.SimpleListItem;
import com.qatestlab.base.BasePage;
import com.macrodroid.pages.elements.DialogWindowAbstract;
import com.qatestlab.base.ElementsListWrapped;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.function.BiPredicate;

/**
 * Created by aptashnik on 12/14/2016.
 */
public abstract class ListDialogWindowAbstract<T extends BasePage<T>> extends DialogWindowAbstract<T> {
    private ElementsListWrapped<SimpleListItem> elementsListWrapped;

    protected ListDialogWindowAbstract() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    @Override
    protected boolean postPageLoadActions() {
        BiPredicate<MobileElement, MobileElement> biPredicate = (arg1, arg2) -> true;
        elementsListWrapped = new ElementsListWrapped<>(driver(), MobileBy.id("select_dialog_listview"), MobileBy.id("text1"),
                RemoteWebElement::getText,
                SimpleListItem::new);
        return true;
    }

    protected ElementsListWrapped<SimpleListItem> getOptionsList() {
        return elementsListWrapped;
    }

    @Override
    protected final By pageLoadedFlag() {
        return MobileBy.id("select_dialog_listview");
    }

    @Override
    protected final int pageLoadTimeout() {
        return 5;
    }
}