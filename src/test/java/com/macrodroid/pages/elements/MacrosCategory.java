package com.macrodroid.pages.elements;

import com.qatestlab.base.BaseTest;
import com.qatestlab.base.ElementsListWrapped;
import com.qatestlab.utils.IWrappedElementsList;
import com.qatestlab.utils.IWrappedMobileElement;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import java.util.function.Predicate;

/**
 * Created by aptashnik on 12/16/2016.
 */
public class MacrosCategory extends SimpleListItem implements IWrappedElementsList<Macros> {

    private MobileElement rootElement,
            categorySwitcher;


    private ElementsListWrapped<Macros> macrosesList;


    private MobileDriver<MobileElement> driver;

    public MacrosCategory(MobileElement element) {
        super(element);
        this.rootElement = element;
        categorySwitcher = rootElement.findElement(MobileBy.className("android.widget.Switch"));
        driver = BaseTest.driver();
        macrosesList = new ElementsListWrapped<>(driver, By.id("macro_list"), MobileBy.id("macro_list_row_container"),
                arg -> arg.findElement(MobileBy.id("macroName")).getText(),
                Macros::new);
    }

    public boolean getCategoryState() {
        return categorySwitcher.getText().toLowerCase().equals("on");
    }

    public boolean setCategoryState(boolean enabled) {
        if(getCategoryState() == enabled)
            return false;
        categorySwitcher.click();
        return true;
    }

    public boolean isMacrosListOpened() {
        MobileElement element;
        try {
            element = driver.findElement(MobileBy.xpath(String.format("//*[contains(@text,'%s')]/../../" +
                    "following-sibling::*", getTitle())));
        }
        catch (NoSuchElementException exc) {
            return false;
        }
        return !element.getAttribute("resourceId").isEmpty();
    }

    public boolean setMacrosListOpened(boolean opened) {
        if(opened == isMacrosListOpened())
            return false;
        rootElement.click();
        return true;
    }

    @Override
    public Macros get(int index) {
        return macrosesList.get(index);
    }

    @Override
    public Macros get(Predicate<Macros> predicate) {
        return macrosesList.get(predicate);
    }

    @Override
    public boolean contains(Predicate<Macros> predicate) {
        return macrosesList.contains(predicate);
    }

    @Override
    public int howManyContains(Predicate<Macros> predicate) {
        return macrosesList.howManyContains(predicate);
    }

    @Override
    public boolean isEmpty() {
        return macrosesList.isEmpty();
    }

    @Override
    public int size() {
        return macrosesList.size();
    }
}
