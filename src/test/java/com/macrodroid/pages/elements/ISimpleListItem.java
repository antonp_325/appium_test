package com.macrodroid.pages.elements;

import com.qatestlab.utils.IWrappedMobileElement;

/**
 * Created by aptashnik on 12/16/2016.
 */
public interface ISimpleListItem extends IWrappedMobileElement {
    String getTitle();
    String getDescription();
}
