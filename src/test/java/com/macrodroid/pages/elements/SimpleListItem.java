package com.macrodroid.pages.elements;

import com.qatestlab.utils.IWrappedMobileElement;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by aptashnik on 12/16/2016.
 */
public class SimpleListItem implements ISimpleListItem {

    private MobileElement wrappedElement;
    private String title, description;

    public SimpleListItem(MobileElement mobileElement) {
        wrappedElement = mobileElement;
        title = wrappedElement.getText();
        if(title.isEmpty()) {
            List<MobileElement> titleAndDescription = wrappedElement.findElements(MobileBy.className("android.widget.TextView"));
            title = titleAndDescription.get(0).getText();
            if(titleAndDescription.size() == 2)
                description  = titleAndDescription.get(1).getText();
            else
                description = "";
        }
        else
            description = "";
    }

    @Override
    public MobileElement getWrappedElement() {
        return wrappedElement;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
