package com.macrodroid.pages.elements;

import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

/**
 * Created by aptashnik on 12/16/2016.
 */
public class CheckBoxListItem extends SimpleListItem implements ICheckBoxListItem {
    private MobileElement checkBox;

    public CheckBoxListItem(MobileElement mobileElement) {
        super(mobileElement);
        try {
            checkBox = mobileElement.findElement(By.className("android.widget.CheckBox"));
        }
        catch (NoSuchElementException ignored) { }
    }

    @Override
    public boolean isCheckable() {
        return checkBox == null || !Boolean.parseBoolean(checkBox.getAttribute("checkable"));
    }

    @Override
    public boolean isChecked() {
        if(checkBox == null)
            return false;
        return Boolean.parseBoolean(checkBox.getAttribute("checked"));
    }

    @Override
    public boolean setState(boolean enabled) {
        if(isChecked() == enabled || !isCheckable())
            return false;
        checkBox.click();
        return true;
    }
}
