package com.macrodroid.pages.elements;

import com.qatestlab.base.BasePage;

/**
 * Created by aptashnik on 12/13/2016.
 */
public interface ISubPageMenu {
    <N extends BasePage<N>> N nextPage(Class<N> subPageClass);
}
