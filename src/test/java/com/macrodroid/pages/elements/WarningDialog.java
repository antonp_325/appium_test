package com.macrodroid.pages.elements;

import com.qatestlab.base.BasePage;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

/**
 * Created by aptashnik on 12/14/2016.
 */
public class WarningDialog extends DialogWindowAbstract<WarningDialog> {

    @Override
    protected By pageLoadedFlag() {
        return MobileBy.id("alertTitle");
    }
}