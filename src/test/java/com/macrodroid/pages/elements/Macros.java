package com.macrodroid.pages.elements;

import com.qatestlab.utils.IMobileElementWrapper;
import com.qatestlab.utils.IWrappedMobileElement;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

/**
 * Created by aptashnik on 12/15/2016.
 */
public class Macros implements IWrappedMobileElement {

    private static final By ENABLE_MACROS_BTN_LOCATOR = MobileBy.id("enabledSwitch");

    private MobileElement element;
    private String name;
    private String[] triggersList;
    private String[] actionsList;
    private String[] constraintsList;

    private MobileElement enableMacrosBtn;

    public Macros(MobileElement element) {
        this.element = element;
        name = element.findElement(MobileBy.id("macroName")).getText();
        triggersList = element.findElement(MobileBy.id("macroTrigger")).getText().split(", ");
        actionsList = element.findElement(MobileBy.id("macroActions")).getText().split(", ");
        constraintsList = element.findElement(MobileBy.id("macroConstraints")).getText().split(", ");
        enableMacrosBtn = element.findElement(ENABLE_MACROS_BTN_LOCATOR);
    }

    public String getName() {
        return name;
    }

    public String[] getTriggersList() {
        return triggersList;
    }

    public String[] getActionsList() {
        return actionsList;
    }

    public String[] getConstraintsList() {
        return constraintsList;
    }

    public boolean isMacrosEnabled() {
        return enableMacrosBtn.getText().toLowerCase().equals("on");
    }

    public void setMacrosEnabled(boolean enable) {
        if(isMacrosEnabled() != enable)
            enableMacrosBtn.click();
    }

    @Override
    public MobileElement getWrappedElement() {
        return element;
    }
}
