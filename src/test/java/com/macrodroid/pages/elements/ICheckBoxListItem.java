package com.macrodroid.pages.elements;

import com.qatestlab.utils.IWrappedMobileElement;

/**
 * Created by aptashnik on 12/16/2016.
 */
public interface ICheckBoxListItem extends IWrappedMobileElement {
    boolean isCheckable();
    boolean isChecked();
    boolean setState(boolean enabled);
}
