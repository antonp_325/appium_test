package com.macrodroid.pages.elements;

import com.qatestlab.base.BasePage;

/**
 * Created by aptashnik on 12/15/2016.
 */
public interface IDialogWindow {
    void accept();
    void cancel();
}
