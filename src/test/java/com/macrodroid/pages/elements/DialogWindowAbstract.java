package com.macrodroid.pages.elements;

import com.qatestlab.base.BasePage;
import com.qatestlab.base.actions.GoBackAction;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by aptashnik on 12/14/2016.
 */
public abstract class DialogWindowAbstract<CPC extends BasePage<CPC>> extends BasePage<CPC> implements IDialogWindow {
    private static final By PAGE_LOCATOR = MobileBy.id("button_ok");
    @AndroidFindAll({
            @AndroidFindBy(id = "button_ok"),
            @AndroidFindBy(id = "button1")
    })
    private MobileElement acceptBtn;

    protected DialogWindowAbstract() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    @Override
    protected By pageLoadedFlag() {
        return PAGE_LOCATOR;
    }

    @Override
    protected int pageLoadTimeout() {
        return 5;
    }

    public void accept() {
        acceptBtn.click();
    }

    public void cancel() {
        new GoBackAction(driver()).perform();
    }
}
