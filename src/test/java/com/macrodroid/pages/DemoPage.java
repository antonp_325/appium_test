package com.macrodroid.pages;

import com.qatestlab.base.BasePage;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by aptashnik on 12/8/2016.
 */
public class DemoPage extends BasePage<DemoPage> {
    private static final String NEXT_BTN_ID = "button_next";


    @AndroidFindBy(id = "button_back")
    private AndroidElement prevPageBtn;

    @AndroidFindBy(id = NEXT_BTN_ID)
    private AndroidElement nextPageBtn;

    public DemoPage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    @Override
    protected By pageLoadedFlag() {
        return MobileBy.id(NEXT_BTN_ID);
    }

    @Override
    protected int pageLoadTimeout() {
        return 5;
    }

    public boolean prev() {
        if(demoSlideN == 0)
            return false;
        prevPageBtn.click();
        demoSlideN--;
        return true;
    }
    private int demoSlideN;
    private boolean nextSlide() {
        if(demoSlideN == 4)
            return false;
        nextPageBtn.click();
        demoSlideN++;
        return true;
    }
    public HomePage skip() {
        while (nextSlide());
        nextPageBtn.click();
        return new HomePage();
    }
}
