package com.macrodroid.pages;

import com.macrodroid.pages.elements.ISubPageMenu;
import com.macrodroid.pages.mainmenu.MacrosListPage;
import com.macrodroid.pages.mainmenu.SettingsPage;
import com.macrodroid.pages.mainmenu.addmacro.TriggersPage;
import com.qatestlab.base.BasePage;
import com.qatestlab.base.BasePageMethodInterceptor;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.springframework.aop.framework.ProxyFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

/**
 * Created by aptashnik on 12/8/2016.
 */
public class HomePage extends BasePage<HomePage> implements ISubPageMenu {
    private static final String MACRO_LIST_BTN_ID = "main_macroListButton";

    @AndroidFindBy(id = MACRO_LIST_BTN_ID)
    private AndroidElement listOfMacrosBtn;

//    @AndroidFindBy(id = "main_macroAddButton")
//    private AndroidElement addMacroBtn;
//
//    @AndroidFindBy(id = "main_settingsButton")
//    private AndroidElement settingsMenuBtn;

    public enum MenuClause {
        MACRO_LIST, ADD_MACRO, SETTINGS;
    }

    private static final HashMap<Class<? extends BasePage<?>>, By> menuClauses;

    static {
        menuClauses = new HashMap<>();
        menuClauses.put(SettingsPage.class, MobileBy.id("main_settingsButton"));
        menuClauses.put(TriggersPage.class, MobileBy.id("main_macroAddButton"));
        menuClauses.put(MacrosListPage.class, MobileBy.id("main_macroListButton"));
    }

    @Override
    protected By pageLoadedFlag() {
        return By.id(MACRO_LIST_BTN_ID);
    }

    @Override
    protected int pageLoadTimeout() {
        return 20;
    }

    public HomePage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    @Override
    public  <N extends BasePage<N>> N nextPage(Class<N> subPageClass) {
        N pageObjectInstance = null;
        try {
            Constructor<N> constructor = subPageClass.getConstructor();
            By pageOpenBtnLocator = menuClauses.get(subPageClass);
            driver().findElement(pageOpenBtnLocator).click();
            pageObjectInstance = constructor.newInstance();

        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return pageObjectInstance;
    }
    public static HomePage getPage() {
        ProxyFactory pf = new ProxyFactory(new HomePage());
        pf.addAdvice(new BasePageMethodInterceptor());
        return (HomePage) pf.getProxy();
    }
    /*
    List<MobileElement> elementList =
        driver().findElements(MobileBy.xpath("//*[contains(@text,'categ')]/../../" +
                "following-sibling::*[preceding-sibling::*[@resource-id=''] and (following-sibling::*[@resource-id=''] or last())]"));
elementList.size();
    */
}
