package com.macrodroid.pages.mainmenu.addmacro;

import com.macrodroid.pages.HomePage;
import com.macrodroid.pages.elements.DialogWindowAbstract;
import com.macrodroid.pages.elements.ISimpleListItem;
import com.macrodroid.pages.elements.SimpleListItem;
import com.qatestlab.base.ElementsListWrapped;
import com.qatestlab.base.actions.GoBackAction;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SaveMacroDialog extends DialogWindowAbstract<SaveMacroDialog> {
    @AndroidFindBy(id = "enter_name_and_category_name")
    private MobileElement macroNameField;
    @AndroidFindBy(id = "enter_name_and_category_spinner")
    private MobileElement openCategoriesListBtn;
    @AndroidFindBy(className = "android.widget.ListView")
    private MobileElement categoriesList;

    private ElementsListWrapped<SimpleListItem> categoriesListWrapped;

    SaveMacroDialog(ConstraintsPage parentPage) {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    public SaveMacroDialog setMacroName(String name) {
        macroNameField.sendKeys(name);
        return this;
    }

    public SaveMacroDialog setMacroCategory(String ctgName) {
        openCategoriesListBtn.click();
        categoriesListWrapped = new ElementsListWrapped<>
                (driver(), MobileBy.className("android.widget.ListView"), MobileBy.id("text1"), RemoteWebElement::getText,
                        SimpleListItem::new);
        ISimpleListItem neededCtg = categoriesListWrapped.get(arg -> arg.getTitle().toLowerCase().equals(ctgName.toLowerCase()));
        if (neededCtg == null)
            new GoBackAction(driver()).perform();
        else
            neededCtg.getWrappedElement().click();
        return this;
    }

    @Override
    public void accept() {
        super.accept();
        MobileElement closeAdd = (MobileElement) new WebDriverWait(driver(), 4)
                .until(ExpectedConditions.presenceOfElementLocated(MobileBy.className("android.widget.ImageButton")));
        closeAdd.click();
    }
}
