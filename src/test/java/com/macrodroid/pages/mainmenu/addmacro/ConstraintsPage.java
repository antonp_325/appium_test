package com.macrodroid.pages.mainmenu.addmacro;

import com.macrodroid.pages.HomePage;
import com.macrodroid.pages.elements.SimpleListItem;
import com.qatestlab.base.BasePage;
import com.qatestlab.base.ElementsListWrapped;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by aptashnik on 12/14/2016.
 */
public class ConstraintsPage extends BasePage<ConstraintsPage> {

//    @AndroidFindBys({
//            @AndroidFindBy(id = "selectConstraintList"),
//            @AndroidFindBy(id = "select_item_row_frame"),
//            @AndroidFindBy(id = "select_item_name")
//    })
//    private List<MobileElement> constraintsList;

    private ElementsListWrapped<SimpleListItem> constraintsListWrapped;

    @AndroidFindBy(id = "info_card_got_it")
    private MobileElement closeTooltipBtn;

    @AndroidFindBy(id = "select_constraint_ok_button")
    private MobileElement acceptActionsBtn;

    public ConstraintsPage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    @Override
    protected By pageLoadedFlag() {
        return MobileBy.id("selectConstraintList");
    }

    @Override
    protected int pageLoadTimeout() {
        return 10;
    }

    @Override
    protected boolean postPageLoadActions() {
        closeTooltipBtn.click();
        constraintsListWrapped = new ElementsListWrapped<>(driver(), MobileBy.id("selectConstraintList"), MobileBy.id("select_item_row_frame"),
                arg -> arg.findElement(MobileBy.className("select_item_name")).getText(),
                SimpleListItem::new);
        return true;
    }
    public SaveMacroDialog confirm() {
        acceptActionsBtn.click();
        return new SaveMacroDialog(this);
    }

}

