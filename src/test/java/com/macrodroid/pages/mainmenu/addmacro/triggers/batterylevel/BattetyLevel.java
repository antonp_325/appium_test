package com.macrodroid.pages.mainmenu.addmacro.triggers.batterylevel;

import com.macrodroid.pages.elements.ListDialogWindowAbstract;
import com.macrodroid.pages.mainmenu.addmacro.TriggersPage;

import java.util.HashMap;

/**
 * Created by aptashnik on 12/14/2016.
 */
public class BattetyLevel extends ListDialogWindowAbstract<BattetyLevel> {

    public enum WhenBatteryLevel {
        INCREASES_OR_DECREASES, ANY_CHANGE;
    }

    private static final HashMap<WhenBatteryLevel, Integer> optionIndexes;

    static {
        optionIndexes = new HashMap<>();
        optionIndexes.put(WhenBatteryLevel.INCREASES_OR_DECREASES, 0);
        optionIndexes.put(WhenBatteryLevel.ANY_CHANGE, 1);
    }

    public BattetyLevel fireBatteryLevelTrigger(WhenBatteryLevel whenBatteryLevel) {
        getOptionsList().get(optionIndexes.get(whenBatteryLevel)).getWrappedElement().click();
        return this;
    }
    public BattetyLevel() {

    }
}

//class BatteryLevelTriggerCase extends ListDialogWindowAbstract<BattetyLevel, TriggersPage> {
//
//    public BatteryLevelTriggerCase(TriggersPage parentPage) {
//        super(parentPage);
//    }
//
//    public void increasesOrDecreasesTo() {
//        getOptionsList().get(0);
//    }
//
//    public void anyChange() {
//        getOptionsList().get(1);
//    }
//}

