package com.macrodroid.pages.mainmenu.addmacro;

import com.macrodroid.pages.HomePage;
import com.macrodroid.pages.elements.ISubPageMenu;
import com.macrodroid.pages.elements.SimpleListItem;
import com.macrodroid.pages.mainmenu.addmacro.triggers.batterylevel.BattetyLevel;
import com.qatestlab.base.BasePage;
import com.qatestlab.base.ElementsListWrapped;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

/**
 * Created by aptashnik on 12/14/2016.
 */
public class TriggersPage extends BasePage<TriggersPage> implements ISubPageMenu {
//    @AndroidFindBys({
//            @AndroidFindBy(id = "selectTriggerList"),
//            @AndroidFindBy(id = "select_item_row_frame"),
//            @AndroidFindBy(id = "select_item_name")
//    })
//    private List<MobileElement> triggersList;

    private static final HashMap<Class<? extends BasePage<?>>, Integer> menuClauseIndexes;

    static {
        menuClauseIndexes = new HashMap<>();
        menuClauseIndexes.put(BattetyLevel.class, 5);
    }

    private ElementsListWrapped<SimpleListItem> triggersListWrapped;
    @AndroidFindBy(id = "info_card_got_it")
    private MobileElement closeTooltipBtn;

    public TriggersPage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    @Override
    protected By pageLoadedFlag() {
        return MobileBy.id("selectTriggerList");
    }

    @Override
    protected int pageLoadTimeout() {
        return 4;
    }

    @Override
    protected boolean postPageLoadActions() {
        triggersListWrapped = new ElementsListWrapped<>(driver(), MobileBy.id("selectTriggerList"), MobileBy.id("select_item_row_frame"),
                arg -> arg.findElement(MobileBy.className("select_item_name")).getText(),
                SimpleListItem::new);
        closeTooltipBtn.click();
        return true;
    }

    @Override
    public <N extends BasePage<N>> N nextPage(Class<N> subPageClass) {
        N pageObjectInstance = null;
        try {
            Constructor<N> constructor = subPageClass.getConstructor();
            int menuClauseIndex = menuClauseIndexes.get(subPageClass);
            triggersListWrapped.get(menuClauseIndex).getWrappedElement().click();
            pageObjectInstance = constructor.newInstance();


        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return pageObjectInstance;
    }

    public <N extends BasePage<N>> N selectTrigger(Class<N> triggerClass) {
        return nextPage(triggerClass);
    }
}
