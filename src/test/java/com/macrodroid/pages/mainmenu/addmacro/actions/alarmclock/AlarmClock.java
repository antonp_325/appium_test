package com.macrodroid.pages.mainmenu.addmacro.actions.alarmclock;

import com.macrodroid.pages.elements.ListDialogWindowAbstract;
import com.macrodroid.pages.mainmenu.addmacro.ActionsPage;
import com.macrodroid.pages.elements.DialogWindowAbstract;
import com.macrodroid.pages.mainmenu.addmacro.TriggersPage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by aptashnik on 12/14/2016.
 */
public class AlarmClock extends ListDialogWindowAbstract<AlarmClock> {

    public enum DoAction {
        SET_ALARM, DISABLE_ALARM
    }


    public AlarmClock(ActionsPage parentPage) {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    public SetAlarmClockLabelDialog disableAlarmWhenTriggerFired() {
        getOptionsList().get(1).getWrappedElement().click();
        accept();
        return new SetAlarmClockLabelDialog();
    }
}

class SetAlarm extends DialogWindowAbstract<SetAlarm> {
}
