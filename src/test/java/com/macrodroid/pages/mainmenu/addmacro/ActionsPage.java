package com.macrodroid.pages.mainmenu.addmacro;

import com.macrodroid.pages.elements.ISubPageMenu;
import com.macrodroid.pages.elements.SimpleListItem;
import com.macrodroid.pages.mainmenu.addmacro.actions.alarmclock.AlarmClock;
import com.qatestlab.base.BasePage;
import com.qatestlab.base.ElementsListWrapped;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

/**
 * Created by aptashnik on 12/14/2016.
 */
public class ActionsPage extends BasePage<ActionsPage> implements ISubPageMenu {
//    @AndroidFindBys({
//            @AndroidFindBy(id = "selectActionList"),
//            @AndroidFindBy(id = "select_item_row_frame"),
//            @AndroidFindBy(id = "select_item_name")
//    })
//    private List<MobileElement> actionsList;

    private static final HashMap<Class<? extends BasePage<?>>, Integer> menuClauseIndexes;

    static {
        menuClauseIndexes = new HashMap<>();
        menuClauseIndexes.put(AlarmClock.class, 1);
    }

    private ElementsListWrapped<SimpleListItem> actionsListWrapped;
    @AndroidFindBy(id = "info_card_got_it")
    private MobileElement closeTooltipBtn;
    @AndroidFindBy(id = "select_action_ok_button")
    private MobileElement acceptActionsBtn;

    public ActionsPage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    @Override
    protected boolean postPageLoadActions() {
        actionsListWrapped = new ElementsListWrapped<>(driver(), MobileBy.id("selectActionList"), MobileBy.id("select_item_row_frame"),
                arg -> arg.findElement(MobileBy.className("select_item_name")).getText(),
                SimpleListItem::new);
        closeTooltipBtn.click();
        return true;
    }

    @Override
    protected By pageLoadedFlag() {
        return MobileBy.id("selectActionList");
    }

    @Override
    protected int pageLoadTimeout() {
        return 5;
    }

    public ConstraintsPage confirm() {
        new WebDriverWait(driver(), 5)
                .until(ExpectedConditions.visibilityOf(acceptActionsBtn));
        acceptActionsBtn.click();
        return new ConstraintsPage();
    }

    @Override
    public <N extends BasePage<N>> N nextPage(Class<N> subPageClass) {
        N pageObjectInstance = null;
        try {
            Constructor<N> constructor = subPageClass.getConstructor(ActionsPage.class);
            constructor.setAccessible(true);
            int menuClauseIndex = menuClauseIndexes.get(subPageClass);
            actionsListWrapped.get(menuClauseIndex).getWrappedElement().click();
            pageObjectInstance = constructor.newInstance(this);


        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return pageObjectInstance;
    }
}
