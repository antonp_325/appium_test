package com.macrodroid.pages.mainmenu.addmacro.triggers.batterylevel;

import com.macrodroid.pages.elements.DialogWindowAbstract;
import com.macrodroid.pages.mainmenu.addmacro.ActionsPage;
import com.macrodroid.pages.mainmenu.addmacro.TriggersPage;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

public class BatteryLevelTriggerSettings extends DialogWindowAbstract<BatteryLevelTriggerSettings> {
    int seekBarX, seekBarWidth;
    @AndroidFindBy(id = "battery_trigger_dialog_decreases_rb")
    private MobileElement decreasesBtn;
    @AndroidFindBy(id = "battery_trigger_dialog_increases_rb")
    private MobileElement increasesBtn;
    @AndroidFindBy(id = "battery_trigger_dialog_seek_bar")
    private AndroidElement seekBar;
    @AndroidFindBy(id = "battery_trigger_dialog_percent_label")
    private MobileElement seekBarProgress;

    public BatteryLevelTriggerSettings() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    public BatteryLevelTriggerSettings setTriggeringMode(TriggerActionsWhenBatteryLevel triggerActionsWhen) {
        switch (triggerActionsWhen) {
            case INCREASES:
                increasesBtn.click();
                break;
            case DECREASES:
                decreasesBtn.click();
                break;
        }
        return this;
    }

    @Override
    protected boolean postPageLoadActions() {
        if (!super.postPageLoadActions())
            return false;
        int y = seekBar.getCenter().getY();
        seekBarX = seekBar.getLocation().getX();
        int seekbarCenterX = seekBar.getCenter().getX();
        seekBarWidth = seekBar.getSize().getWidth();

        String prevProgressStatus = seekBarProgress.getText();
        int stepToNextPercentValue = 0;
        final int delta = (int) (driver().manage().window().getSize().getWidth() * 0.01);

        do {
            stepToNextPercentValue += delta;
            new TouchAction(driver()).tap(seekBar.getLocation().getX(), y).waitAction(1000).perform();
            new TouchAction(driver()).tap(seekbarCenterX + stepToNextPercentValue, y).perform();
        } while (prevProgressStatus.equals(seekBarProgress.getText()));

        int beginDZoneOffset = 0;
        prevProgressStatus = "0%";
        do {
            new TouchAction(driver()).tap(seekBar.getLocation().getX() + seekBarWidth - 1, y).perform();
            new TouchAction(driver()).tap(seekBarX + beginDZoneOffset + stepToNextPercentValue, y).perform();
            if (!prevProgressStatus.equals(seekBarProgress.getText()))
                break;
            else
                beginDZoneOffset += delta;
        } while (true);

        int endDZoneOffset = 0;
        prevProgressStatus = "100%";
        do {
            new TouchAction(driver()).tap(seekBar.getLocation().getX(), y).perform();
            new TouchAction(driver()).tap(seekBarX + seekBarWidth - endDZoneOffset - stepToNextPercentValue, y).perform();
            if (!prevProgressStatus.equals(seekBarProgress.getText()))
                break;
            else
                endDZoneOffset += delta;
        } while (true);

        seekBarX += beginDZoneOffset;
        seekBarWidth -= beginDZoneOffset + endDZoneOffset;
        return true;
    }

    public BatteryLevelTriggerSettings setSeekBarPercent(int percent) {
        new TouchAction(driver()).tap((int) Math.ceil(seekBarX + (seekBarWidth - 1) * (percent / 100.)), seekBar.getCenter().getY()).perform();
        return this;
    }

    @Override
    protected By pageLoadedFlag() {
        return MobileBy.id("battery_trigger_dialog_decreases_rb");
    }

    @Override
    protected int pageLoadTimeout() {
        return 3;
    }

    enum TriggerActionsWhenBatteryLevel {
        INCREASES, DECREASES;
    }
}