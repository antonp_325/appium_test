package com.macrodroid.pages.mainmenu.addmacro.actions.alarmclock;

import com.macrodroid.pages.elements.DialogWindowAbstract;
import com.macrodroid.pages.elements.WarningDialog;
import com.macrodroid.pages.mainmenu.addmacro.ActionsPage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by aptashnik on 12/15/2016.
 */
public class SetAlarmClockLabelDialog extends DialogWindowAbstract<SetAlarmClockLabelDialog> {
    @AndroidFindBy(id = "dialog_dismiss_alarm_label")
    private MobileElement alarmNameField;

    protected SetAlarmClockLabelDialog() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    public SetAlarmClockLabelDialog setAlarmClockLabel(String label) {
        alarmNameField.sendKeys(label);
        return this;
    }

    @Override
    public void accept() {
        super.accept();
        try {
            WarningDialog warningPopup = new WarningDialog().get();
            warningPopup.accept();
        } catch (Error ignored) {
        }
    }
}
