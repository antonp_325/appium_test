package com.macrodroid.pages.mainmenu;

import com.macrodroid.pages.HomePage;
import com.macrodroid.pages.elements.MacrosCategory;
import com.qatestlab.base.BasePage;
import com.qatestlab.base.ElementsListWrapped;
import com.qatestlab.utils.IWrappedElementsList;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

import java.util.function.Predicate;

/**
 * Created by aptashnik on 12/15/2016.
 */
public class MacrosListPage extends BasePage<MacrosListPage>  implements IWrappedElementsList<MacrosCategory> {
    //@AndroidFindBy(id = "macro_list_row_container")
    //private List<MobileElement> macroList;

    private ElementsListWrapped<MacrosCategory> categoriesList;

    @Override
    protected By pageLoadedFlag() {
        return MobileBy.id("macro_list_add_button");
    }

    @Override
    protected boolean postPageLoadActions() {
        categoriesList = new ElementsListWrapped<>(driver(), MobileBy.id("macro_list"), MobileBy.id("group_item_container"),
                mobileElement -> mobileElement.findElement(MobileBy.id("group_title")).getText(),
                MacrosCategory::new);

        //xpath //android.widget.LinearLayout[@resource-id='']
        return true;
    }

    @Override
    protected int pageLoadTimeout() {
        return 5;
    }

    @Override
    public MacrosCategory get(int index) {
        try {
            for (int i = 0; i < size(); i++)
                if (i != index)
                    categoriesList.get(i).setMacrosListOpened(false);
        }
        catch (IndexOutOfBoundsException ignored) { }
        MacrosCategory macrosCategory = categoriesList.get(index);
        macrosCategory.setMacrosListOpened(true);
        return macrosCategory;
    }

    @Override
    public MacrosCategory get(Predicate<MacrosCategory> predicate) {
        return null;
    }

    @Override
    public boolean contains(Predicate<MacrosCategory> predicate) {
        return false;
    }

    @Override
    public int howManyContains(Predicate<MacrosCategory> predicate) {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public int size() {
        return 0;
    }
}
