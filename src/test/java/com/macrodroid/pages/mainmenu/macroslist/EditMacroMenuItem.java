package com.macrodroid.pages.mainmenu.macroslist;

import com.macrodroid.pages.elements.ISimpleListItem;
import com.macrodroid.pages.elements.SimpleListItem;
import com.qatestlab.base.BaseTest;
import com.qatestlab.base.ElementsListWrapped;
import com.qatestlab.utils.IWrappedElementsList;
import com.qatestlab.utils.IWrappedMobileElement;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;

import java.util.function.Predicate;

/**
 * Created by aptashnik on 12/20/2016.
 */
public class EditMacroMenuItem implements ISimpleListItem, IWrappedElementsList<SimpleListItem> {

    private ElementsListWrapped<SimpleListItem> optionsList;

    private final MobileElement element;
    EditMacroMenuItem(MobileElement element) {
        this.element = element;
        optionsList = new ElementsListWrapped<>(BaseTest.driver(),
                MobileBy.xpath(String.format("//android.widget.LinearLayout//*[contains(@text='%s')]/../../android.widget.ListView/*", getTitle())),
                MobileBy.className("android.view.ViewGroup"),
                arg -> arg.findElement(MobileBy.id("macro_edit_entry_name")).getText(), SimpleListItem::new);
    }

    @Override
    public MobileElement getWrappedElement() {
        return element;
    }


    @Override
    public String getTitle() {
        return element.findElements(MobileBy.className("android.widget.TextView")).get(0).getText();
    }

    @Override
    public String getDescription() {
        throw new UnsupportedOperationException();
    }

    @Override
    public SimpleListItem get(int index) {
        return optionsList.get(index);
    }

    @Override
    public SimpleListItem get(Predicate<SimpleListItem> predicate) {
        return optionsList.get(predicate);
    }

    @Override
    public boolean contains(Predicate<SimpleListItem> predicate) {
        return optionsList.contains(predicate);
    }

    @Override
    public int howManyContains(Predicate<SimpleListItem> predicate) {
        return optionsList.howManyContains(predicate);
    }

    @Override
    public boolean isEmpty() {
        return optionsList.isEmpty();
    }

    @Override
    public int size() {
        return optionsList.size();
    }
}
