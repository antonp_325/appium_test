package com.macrodroid.pages.mainmenu.macroslist;

import com.macrodroid.pages.mainmenu.MacrosListPage;
import com.qatestlab.base.BasePage;
import com.qatestlab.base.ElementsListWrapped;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by aptashnik on 12/20/2016.
 */
public class EditMacrosPage extends BasePage<EditMacrosPage> {
    //private ElementsListWrapped<Void>

    @AndroidFindBy(id = "menu_delete")
    private MobileElement removeMacrosBtn;
    @AndroidFindBy(id = "edit_macro_category_button")
    private MobileElement changeMacrosCategoryBtn;

    public EditMacrosPage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    @Override
    protected By pageLoadedFlag() {
        return MobileBy.id("edit_macro_container");
    }

    @Override
    protected int pageLoadTimeout() {
        return 4;
    }

    @Override
    protected boolean postPageLoadActions() {

        return true;
    }
}
