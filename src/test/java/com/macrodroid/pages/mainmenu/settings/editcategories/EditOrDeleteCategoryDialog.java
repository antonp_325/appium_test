package com.macrodroid.pages.mainmenu.settings.editcategories;

import com.macrodroid.pages.mainmenu.settings.EditCategoriesPage;
import com.macrodroid.pages.elements.DialogWindowAbstract;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AndroidFindBys;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by aptashnik on 12/19/2016.
 */
public class EditOrDeleteCategoryDialog extends DialogWindowAbstract<EditOrDeleteCategoryDialog> {

    @AndroidFindBys({
            @AndroidFindBy(id = "select_dialog_listview"),
            @AndroidFindBy(className = "android.widget.TextView")
    })
    private MobileElement editCategoryBtn;
    @AndroidFindBy(xpath = "//*[@resource-id='select_dialog_listview']/android.widget.TextView[2]")
    private MobileElement deleteCategoryBtn;

    public EditOrDeleteCategoryDialog() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    public EditCategoryNameDialog editCategory() {
        editCategoryBtn.click();
        return new EditCategoryNameDialog();
    }

    public void deleteCategory() {
        deleteCategoryBtn.click();
    }

    @Override
    protected By pageLoadedFlag() {
        return MobileBy.id("select_dialog_listview");
    }

    @Override
    public void accept() {
        throw new UnsupportedOperationException();
    }
}
