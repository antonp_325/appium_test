package com.macrodroid.pages.mainmenu.settings.editcategories;

import com.macrodroid.pages.elements.ISimpleListItem;
import com.macrodroid.pages.elements.SimpleListItem;
import com.macrodroid.pages.mainmenu.settings.EditCategoriesPage;
import com.qatestlab.utils.IWrappedMobileElement;
import io.appium.java_client.MobileElement;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by aptashnik on 12/19/2016.
 */
public class Category implements IWrappedMobileElement, ISimpleListItem {
    private ISimpleListItem category;
    private EditOrDeleteCategoryDialog nextPage;

    public Category(ISimpleListItem element, EditOrDeleteCategoryDialog nextPage) {
        category = element;
        this.nextPage = nextPage;
    }

    @Override
    public MobileElement getWrappedElement() {
        return category.getWrappedElement();
    }

    public EditOrDeleteCategoryDialog click() {
        getWrappedElement().click();
        return nextPage;
    }

    @Override
    public String getTitle() {
        return category.getTitle();
    }

    @Override
    public String getDescription() {
        throw new UnsupportedOperationException();
    }
}
