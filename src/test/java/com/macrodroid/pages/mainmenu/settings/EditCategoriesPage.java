package com.macrodroid.pages.mainmenu.settings;

import com.macrodroid.pages.elements.ISimpleListItem;
import com.macrodroid.pages.elements.SimpleListItem;
import com.macrodroid.pages.mainmenu.SettingsPage;
import com.macrodroid.pages.mainmenu.settings.editcategories.Category;
import com.macrodroid.pages.mainmenu.settings.editcategories.EditCategoryNameDialog;
import com.macrodroid.pages.mainmenu.settings.editcategories.EditOrDeleteCategoryDialog;
import com.qatestlab.base.BasePage;
import com.qatestlab.base.ElementsListWrapped;
import com.qatestlab.utils.IWrappedElementsList;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.function.Predicate;

/**
 * Created by aptashnik on 12/19/2016.
 */
public class EditCategoriesPage extends BasePage<EditCategoriesPage> implements IWrappedElementsList<ISimpleListItem> {

    private ElementsListWrapped<ISimpleListItem> categoriesList;

    @AndroidFindBy(id = "menu_add")
    private MobileElement newCategoryBtn;

    public EditCategoriesPage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    @Override
    protected By pageLoadedFlag() {
        return MobileBy.id("edit_categories_list");
    }

    @Override
    protected boolean postPageLoadActions() {
        categoriesList = new ElementsListWrapped<>(driver(), MobileBy.id("edit_categories_list"), MobileBy.id("text1"), RemoteWebElement::getText,
                SimpleListItem::new);
        return true;
    }

    @Override
    protected int pageLoadTimeout() {
        return 4;
    }
    public EditCategoryNameDialog newCategory() {
        newCategoryBtn.click();
        return new EditCategoryNameDialog();
    }

    @Override
    public Category get(int index) {
        return new Category(categoriesList.get(index), new EditOrDeleteCategoryDialog());
    }

    @Override
    public ISimpleListItem get(Predicate<ISimpleListItem> predicate) {
        return categoriesList.get(predicate);
    }

    @Override
    public boolean contains(Predicate<ISimpleListItem> predicate) {
        return categoriesList.contains(predicate);
    }

    @Override
    public int howManyContains(Predicate<ISimpleListItem> predicate) {
        return categoriesList.howManyContains(predicate);
    }

    @Override
    public boolean isEmpty() {
        return categoriesList.isEmpty();
    }

    @Override
    public int size() {
        return categoriesList.size();
    }
}