package com.macrodroid.pages.mainmenu.settings.editcategories;

import com.macrodroid.pages.mainmenu.settings.EditCategoriesPage;
import com.macrodroid.pages.elements.DialogWindowAbstract;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by aptashnik on 12/19/2016.
 */
public class EditCategoryNameDialog extends DialogWindowAbstract<EditCategoryNameDialog> {

    @AndroidFindBy(className = "android.widget.EditText")
    private MobileElement categoryNameField;

    public EditCategoryNameDialog() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }
    public EditCategoryNameDialog setCategoryName(String newName) {
        categoryNameField.sendKeys(newName);
        return this;
    }
}
