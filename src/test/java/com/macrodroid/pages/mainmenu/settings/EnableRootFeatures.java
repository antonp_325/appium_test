package com.macrodroid.pages.mainmenu.settings;

import com.macrodroid.pages.mainmenu.SettingsPage;
import com.qatestlab.base.BasePage;
import org.openqa.selenium.By;

/**
 * Created by aptashnik on 12/13/2016.
 */
public class EnableRootFeatures extends BasePage<EnableRootFeatures> {

    @Override
    protected By pageLoadedFlag() {
        return null;
    }

    @Override
    protected int pageLoadTimeout() {
        return 0;
    }

    @Override
    public EnableRootFeatures get() {
        return this;
    }
}
