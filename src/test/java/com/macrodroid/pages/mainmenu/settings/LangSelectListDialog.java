package com.macrodroid.pages.mainmenu.settings;

import com.macrodroid.pages.HomePage;
import com.macrodroid.pages.elements.ListDialogWindowAbstract;
import com.macrodroid.pages.mainmenu.SettingsPage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by aptashnik on 12/12/2016.
 */
public class LangSelectListDialog extends ListDialogWindowAbstract<LangSelectListDialog> {

//    @AndroidFindBys({
//            @AndroidFindBy(id = "parentPanel"),
//            @AndroidFindBy(className = "android.widget.CheckedTextView")
//    })
//    private List<MobileElement> langClauses;

    public LangSelectListDialog() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    public HomePage selectLang(String lang) {
        getOptionsList().get((arg) -> arg.getTitle().toLowerCase().equals(lang)).getWrappedElement().click();
        return new HomePage();
    }

}
