package com.macrodroid.pages.mainmenu;

import com.macrodroid.pages.HomePage;
import com.macrodroid.pages.elements.ISubPageMenu;
import com.macrodroid.pages.elements.SimpleListItem;
import com.macrodroid.pages.mainmenu.settings.EditCategoriesPage;
import com.macrodroid.pages.mainmenu.settings.EnableRootFeatures;
import com.macrodroid.pages.mainmenu.settings.LangSelectListDialog;
import com.qatestlab.base.BasePage;
import com.qatestlab.base.ElementsListWrapped;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.PageFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

/**
 * Created by aptashnik on 12/8/2016.
 */
public class SettingsPage extends BasePage<SettingsPage> implements ISubPageMenu {
    private static final HashMap<Class<? extends BasePage<?>>, Integer> menuClauseIndexes;

    static {
        menuClauseIndexes = new HashMap<>();
        menuClauseIndexes.put(LangSelectListDialog.class, 15);
        menuClauseIndexes.put(EnableRootFeatures.class, 1);
        menuClauseIndexes.put(EditCategoriesPage.class, 4);
    }
    private ElementsListWrapped<SimpleListItem> elementsListWrapped;

    @AndroidFindBy(id = "hide_text")
    private MobileElement hideSocialButtonsBtn;

    public SettingsPage() {
        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
    }

    @Override
    public By pageLoadedFlag() {
        return By.id("content");
    }

    @Override
    public int pageLoadTimeout() {
        return 5;
    }


    @Override
    protected boolean postPageLoadActions() {
        hideSocialButtonsBtn.click();
        elementsListWrapped = new ElementsListWrapped<>(driver(), MobileBy.id("list"), MobileBy.className("android.widget.RelativeLayout"),
                RemoteWebElement::getText,
                SimpleListItem::new);
        return true;
    }

    @Override
    public <N extends BasePage<N>> N nextPage(Class<N> subPageClass) {
        N pageObjectInstance = null;
        try {
            Constructor<N> constructor = subPageClass.getConstructor();
            int menuClauseIndex = menuClauseIndexes.get(subPageClass);
            elementsListWrapped.get(menuClauseIndex).getWrappedElement().click();
            pageObjectInstance = constructor.newInstance();


        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return pageObjectInstance;
    }
}
