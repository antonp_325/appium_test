package com.macrodroid.pages.mainmenu;

import com.macrodroid.pages.HomePage;
import com.macrodroid.pages.mainmenu.addmacro.TriggersPage;
import com.qatestlab.base.BasePage;
import org.openqa.selenium.By;

/**
 * Created by aptashnik on 12/15/2016.
 */
public class AddMacroPage extends BasePage<TriggersPage> {
    private TriggersPage triggersPage;

    public AddMacroPage() {
        triggersPage = new TriggersPage();
    }

    @Override
    public TriggersPage get() {
        return triggersPage.get();
    }

    @Override
    protected int pageLoadTimeout() {
        return 0;
    }
    @Override
    protected By pageLoadedFlag() {
        return null;
    }
}
