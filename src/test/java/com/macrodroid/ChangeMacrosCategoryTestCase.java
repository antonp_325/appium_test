package com.macrodroid;

import com.macrodroid.pages.DemoPage;
import com.macrodroid.pages.HomePage;
import com.macrodroid.pages.elements.SimpleListItem;
import com.macrodroid.pages.mainmenu.SettingsPage;
import com.macrodroid.pages.mainmenu.settings.EditCategoriesPage;
import com.macrodroid.pages.mainmenu.settings.editcategories.Category;
import com.macrodroid.pages.mainmenu.settings.editcategories.EditCategoryNameDialog;
import com.macrodroid.pages.mainmenu.settings.editcategories.EditOrDeleteCategoryDialog;
import com.qatestlab.base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by aptashnik on 12/19/2016.
 */
public class ChangeMacrosCategoryTestCase extends BaseTest {
    @Test
    public void test() {
        HomePage homePage = new DemoPage().get().skip().get();
        SettingsPage settingsPage = homePage.nextPage(SettingsPage.class).get();
        EditCategoriesPage editCategoriesPage = settingsPage.nextPage(EditCategoriesPage.class).get();

        Category category = editCategoriesPage.get(0);

        String oldCategoryName = category.getTitle();
        EditCategoryNameDialog categoryNameDialog = category.click().get().editCategory().get();
        String newCategoryName = "Hight performance";
        categoryNameDialog.setCategoryName(newCategoryName).accept();
        editCategoriesPage.get();
        Assert.assertFalse(editCategoriesPage.contains(arg -> arg.getTitle().equals(oldCategoryName)));
        Assert.assertTrue(editCategoriesPage.contains(arg -> arg.getTitle().equals(newCategoryName)));
    }
}
