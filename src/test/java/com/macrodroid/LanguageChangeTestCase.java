package com.macrodroid;

import com.macrodroid.pages.DemoPage;
import com.macrodroid.pages.mainmenu.settings.LangSelectListDialog;
import com.macrodroid.pages.HomePage;
import com.macrodroid.pages.mainmenu.SettingsPage;
import com.qatestlab.base.BaseTest;
import org.testng.annotations.Test;

/**
 * Created by aptashnik on 12/8/2016.
 */

public class LanguageChangeTestCase extends BaseTest {
    @Test
    public void firstTest() {
        DemoPage demoPage = new DemoPage().get();
        HomePage homePage = demoPage.skip().get();
        SettingsPage settingsPage = homePage.nextPage(SettingsPage.class).get();
        LangSelectListDialog langSelectPopup = settingsPage.nextPage(LangSelectListDialog.class).get();
        langSelectPopup.selectLang("svenska").get();
    }
}


//    public ForgotPasswordPage() {
//        PageFactory.initElements(new AppiumFieldDecorator(driver()), this);
//    }