package com.qatestlab.utils;

import com.qatestlab.properties.Properties;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.ServerArgument;

import java.io.File;
import java.net.URL;
import java.nio.file.Paths;

/**
 * Created by aptashnik on 12/9/2016.
 */
public class AppiumServer {

    private AppiumDriverLocalService service = null;
    public String deviceName;

    static {
//        System.setProperty(AppiumServiceBuilder.NODE_PATH, "C:/Program Files (x86)/Appium/node.exe");
//        System.setProperty(AppiumServiceBuilder.APPIUM_PATH, "C:/Program Files (x86)/Appium/node_modules/appium/lib/appium.js");
    }

//    public AppiumServer() {
//        this("127.0.0.1", 4723);
//    }

    public AppiumServer() {
        service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
                .withIPAddress("127.0.0.1")
                .usingAnyFreePort()
                .withArgument(() -> "--full-reset")
                .withArgument(() -> "--session-override")
                .withArgument(() -> "--command-timeout", "3600")
                .withArgument(() -> "--launch-timeout", "300000")
                .withArgument(() -> "--native-instruments-lib")
                .usingDriverExecutable(new File(Paths.get(Properties.getAppiumHome(), "node.exe").toAbsolutePath().toString()))
                .withAppiumJS(new File(Paths.get(Properties.getAppiumHome(), "node_modules", "appium", "bin", "appium.js").toAbsolutePath().toString())));
    }

    public void startAppiumServer() {
        service.start();
    }

    public void stopAppiumServer() {
        if (service.isRunning()) {
            service.stop();
        }
    }

    public boolean isServerRunning() {
        return service.isRunning();
    }

    public URL getURL() {
        return service.getUrl();
    }

}
