package com.qatestlab.utils;

/**
 * Created by andro on 11/21/2016.
 */
public class Timeouts {
    public static final short HOME_PAGE = 16,
            PERSONAL_PAGE = 10,
            GROUP_PAGE = 10,
            TWEET_COMMENT_PAGE = 5,
            TWEET_CONTAINER_UPDATE = 30,
            PAGE_WITH_TWEETS = 10,
            TOP_MENU = 5,
            TWEET_COMMENT_DISAPPEAR = 7,
            TWEET_CONTAINER_NEWTWEETS_BTN_APPEAR = 6,
            INVALID_LOGIN_PAGE = 4;

}