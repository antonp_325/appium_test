package com.qatestlab.utils;

/**
 * Created by aptashnik on 12/9/2016.
 */
public enum Direction {
    LEFT, RIGHT, DOWN, UP
}
