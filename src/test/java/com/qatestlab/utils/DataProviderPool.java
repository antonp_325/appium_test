package com.qatestlab.utils;

import org.testng.annotations.DataProvider;

public class DataProviderPool {

    // Constants must match the name attributes for <credentials/> tags in datapool.xml

    public static final String LOGIN_TEST_CASE_DATAPROVIDER = "loginTC_loginDataProvider";
    public static final String RETWEET_TEST_CASE_LOGIN_PROVIDER = "retweetTC_loginDataProvider";

    private static DataPoolReader reader = new DataPoolReader();

    @DataProvider(name = LOGIN_TEST_CASE_DATAPROVIDER)
    public static Object[][] loginDataProvider() {
        return reader.GetCredentials(LOGIN_TEST_CASE_DATAPROVIDER);
    }

    @DataProvider(name = RETWEET_TEST_CASE_LOGIN_PROVIDER)
    public static Object[][] retweetTestCaseLoginProvider() {
        return reader.GetCredentials(RETWEET_TEST_CASE_LOGIN_PROVIDER);
    }
}
