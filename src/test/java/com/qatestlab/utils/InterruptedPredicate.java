package com.qatestlab.utils;

/**
 * Created by andro on 11/16/2016.
 */
public interface InterruptedPredicate<T> {
    public boolean test(T t) throws InterruptedException;
}