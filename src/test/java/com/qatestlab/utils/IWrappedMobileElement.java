package com.qatestlab.utils;

import io.appium.java_client.MobileElement;

/**
 * Created by aptashnik on 12/15/2016.
 */
public interface IWrappedMobileElement {
    MobileElement getWrappedElement();
}
