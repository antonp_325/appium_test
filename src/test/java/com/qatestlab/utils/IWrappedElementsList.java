package com.qatestlab.utils;

import java.util.function.Predicate;

/**
 * Created by aptashnik on 12/19/2016.
 */
public interface IWrappedElementsList<T extends IWrappedMobileElement> {
    T get(int index);
    T get(Predicate<T> predicate);
    boolean contains(Predicate<T> predicate);
    int howManyContains(Predicate<T> predicate);
    boolean isEmpty();
    int size();
}