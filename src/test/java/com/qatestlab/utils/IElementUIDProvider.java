package com.qatestlab.utils;

import io.appium.java_client.MobileElement;

import java.util.function.Function;

/**
 * Created by aptashnik on 12/16/2016.
 */
public interface IElementUIDProvider extends Function<MobileElement, String> {
}
