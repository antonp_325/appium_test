package com.qatestlab.properties;

import java.nio.file.Paths;

/**
 * Created by andro on 11/21/2016.
 */
public class Properties {
    private static final String OS_NAME,
            OS_VERSION,
            BROWSER_NAME,
            WEBDRIVERS_DIR,
    CONFIG_DIR_PATH,
    BUILD_ATTIFACTS_DIR_PATH,
    ALLURE_RESULTS_DIR_PATH,
    APPIUM_HOME,
    APPIUM_SERVER_ADDRESS,
    AUT_PATH;
    private static final boolean IS_LINUX;

    static {
        OS_NAME = System.getProperty("os.name");
        OS_VERSION = System.getProperty("os.version");
        BROWSER_NAME = System.getProperty("browser");
        IS_LINUX = !OS_NAME.toLowerCase().startsWith("windows");
        WEBDRIVERS_DIR = Paths.get("web_drivers/" +
            (IS_LINUX ? "linux" : "windows") + "/")
            .toAbsolutePath().toString() + "/";
        CONFIG_DIR_PATH = System.getProperty("configDir.path");
        BUILD_ATTIFACTS_DIR_PATH = "target";
        ALLURE_RESULTS_DIR_PATH = System.getProperty("allure.results.directory");
        APPIUM_HOME = System.getenv("APPIUM_HOME");
        APPIUM_SERVER_ADDRESS = System.getProperty("appiumServerAddress");
        AUT_PATH = System.getProperty("autPath");
    }

    public static boolean isLinux() {
        return IS_LINUX;
    }
    public static String getOsName() {
        return OS_NAME;
    }
    public static String getOsVersion() { return  OS_VERSION; }
    public static String getBrowserName() {
        return BROWSER_NAME;
    }
    public static String getWebdriversDir() {
        return WEBDRIVERS_DIR;
    }
    public static String getConfigDirPath() { return CONFIG_DIR_PATH; }
    public static String getAllureResultsDirPath() { return ALLURE_RESULTS_DIR_PATH;}
    public static String getAppiumHome() { return APPIUM_HOME; }
    public static String getAppiumServerAddress() { return APPIUM_SERVER_ADDRESS; }
    public static String getAutPath() { return AUT_PATH; }
}