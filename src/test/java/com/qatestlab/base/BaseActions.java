package com.qatestlab.base;

import com.qatestlab.base.actions.GoBackAction;
import com.qatestlab.base.actions.SwipeByPercentAction;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.interactions.Actions;

/**
 * Created by aptashnik on 12/9/2016.
 */
public class BaseActions extends Actions {
    private final MobileDriver<MobileElement> driver;

    public BaseActions(MobileDriver<MobileElement> driver) {
        super(driver);
        this.driver = driver;
    }

    public BaseActions goBack() {
        action.addAction(new GoBackAction(driver));
        return this;
    }

    public BaseActions swipe(com.qatestlab.utils.Direction direction, int xPerc, int yPerc, int offsetPerc) {
        action.addAction(new SwipeByPercentAction(driver, direction, xPerc, yPerc, offsetPerc));
        return this;
    }
}
