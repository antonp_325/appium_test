package com.qatestlab.base;

import com.qatestlab.utils.NotInjectedWebDriverException;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import org.aopalliance.aop.Advice;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.aop.framework.ProxyFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Created by andro on 11/14/2016.
 */
public abstract class BasePage<CPC extends BasePage<CPC>> extends LoadableComponent<CPC> {
    private boolean postLoadMethodCalled, postLoadMethodSuccess;

    public BasePage() {
        if (driver() == null)
            throw new NotInjectedWebDriverException();
//        ProxyFactory pf = new ProxyFactory(this);
//        pf.addAdvice((MethodInterceptor) methodInvocation -> {
//            BasePage<CPC, PPC> obj = (BasePage<CPC, PPC>) methodInvocation.getThis();
//            return null;
//        });

    }

    public static void scroolPageTo(WebElement webElement) {
        throw new UnsupportedOperationException();
    }

    protected static MobileDriver<MobileElement> driver() {
        //return webDriverThreadLocal.getInteger();
        return BaseTest.driver();
    }

    private boolean isElementDisplayed(int timeout, By elementLocator) {
        try {
            new WebDriverWait(driver(), timeout)
                    .until(ExpectedConditions.visibilityOfElementLocated(elementLocator));
        } catch (TimeoutException te) {
            return false;
        }
        return true;
    }

    protected abstract By pageLoadedFlag();

    protected abstract int pageLoadTimeout();

    protected boolean postPageLoadActions() {
        return true;
    }

    @Override
    protected void load() {
        if (isElementDisplayed(pageLoadTimeout(), pageLoadedFlag()))
            if (!postLoadMethodCalled) {
                postLoadMethodSuccess = postPageLoadActions();
                postLoadMethodCalled = true;
            }
    }

    @Override
    protected void isLoaded() throws Error {
        if (!(isElementDisplayed(0, pageLoadedFlag())
                && postLoadMethodCalled && postLoadMethodSuccess))
            throw new Error(String.format("Page %s hasn't loaded", getClass().getName()));
    }
}