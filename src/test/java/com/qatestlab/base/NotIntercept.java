package com.qatestlab.base;

import java.lang.annotation.Retention;

/**
 * Created by aptashnik on 12/21/2016.
 */
@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface NotIntercept {
}
