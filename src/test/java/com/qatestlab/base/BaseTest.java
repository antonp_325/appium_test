package com.qatestlab.base;

import com.qatestlab.properties.Properties;
import com.qatestlab.utils.AppiumServer;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by aptashnik on 12/8/2016.
 */
public class BaseTest {
    private static final ThreadLocal<MobileDriver<MobileElement>> driverThreadLocal = new ThreadLocal<>();
    private static final DesiredCapabilities capabilities = new DesiredCapabilities();
    private static final ThreadLocal<AppiumServer> appiumServer = new ThreadLocal<>();
    //private static int appiumPort = 4723;
    private static ThreadLocal<Integer> appiumPort = new ThreadLocal<>();
    private static AtomicInteger appiumPortSource = new AtomicInteger(4723);

    public static MobileDriver<MobileElement> driver() {
        return driverThreadLocal.get();
    }

    public static AppiumServer appiumServer() {
        return appiumServer.get();
    }

    public static void stopAvdEmulator() {
        try {
            Process p = Runtime.getRuntime().exec("tasklist");
            BufferedReader reader = new
                    BufferedReader(new InputStreamReader(p.getInputStream()));
            String emulatorProcessPartialName = "qemu-system";
            String line;
            String processToKill;
            while ((line = reader.readLine()) != null) {
                if (line.contains(emulatorProcessPartialName)) {
                    processToKill = line.split("\\s")[0].split(".exe")[0];
                    System.err.println("Killing process: " + processToKill);
                    Runtime.getRuntime().exec("tskill " + processToKill);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @BeforeSuite
    public void initSuite() {


        //startAvdEmulator();
    }

    @BeforeTest
    public void setUp() throws MalformedURLException {
        appiumPort.set(appiumPortSource.getAndAdd(100));
        //driver = new AndroidDriver<>(new URL(address), capabilities);
        //BasePage.setWebDriver(new AndroidDriver<>(new URL(ADDRESS), capabilities));

        //appiumServer.set(new AppiumServer("127.0.0.1", appiumPort.get()));
        appiumServer.set(new AppiumServer());

        //new AndroidDriver<>(new URL(address), capabilities);
        //capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.ANDROID);
        //capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, osVersion);

        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Emulator");

        //capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
        capabilities.setCapability(MobileCapabilityType.APP, Properties.getAutPath());


        appiumServer().stopAppiumServer();
        appiumServer().startAppiumServer();


//        driverThreadLocal.set(new AndroidDriver<>(new URL(ADDRESS), capabilities));
        driverThreadLocal.set(new AndroidDriver<>(appiumServer().getURL(), capabilities));
    }

    @AfterTest
    public void tearDown() {
        //driver.quit();
        //BasePage.driver().quit();
        driver().quit();
        appiumServer().stopAppiumServer();
    }

    @AfterSuite
    private void stopAppiumServer() {
    }

    private void startAvdEmulator() {
        String timeout = "20";//Properties.getAppiumEmulatorTimeout();
        //System.out.println("Starting emulator for '" + Properties.getAppiumDeviceName() + "'.");
        String[] aCommand = new String[]{"C:\\Users\\aptashnik\\AppData\\Local\\Android\\android-sdk\\tools\\emulator.exe", "-avd", "AVD_for_Nexus_5_by_Google"};
        try {
            Process process = new ProcessBuilder(aCommand).start();
            if (timeout != null && !timeout.isEmpty()) {
                process.waitFor(Integer.parseInt(timeout), TimeUnit.SECONDS);
            } else {
                process.waitFor(30, TimeUnit.SECONDS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /*    private void restartAdbServer() {
            String[] killServer = {"adb", "kill-server"};
            String[] startServer = {"adb", "start-server"};
            try {
                new ProcessBuilder(killServer).start().waitFor();
                new ProcessBuilder(startServer).start().waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    */
    private void executeAdbConnect(String deviceName) {
        //String udid = "emulator-5556";
        if (null != deviceName) {
            try {
                String[] adb_cmd = {"adb", "connect", deviceName};
                int pstatus = new ProcessBuilder(adb_cmd).start().waitFor();
                System.out.println("Successfully executed adb connect. Process ended with status: " + pstatus);
            } catch (Exception e) {
                System.out.println("Failure while attempting adb connect.");
                e.printStackTrace();
            }
        } else {
            System.out.println("Device UDID is null!");
        }
    }
}