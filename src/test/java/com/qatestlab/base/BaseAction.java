package com.qatestlab.base;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.interactions.Action;

/**
 * Created by aptashnik on 12/13/2016.
 */
public abstract class BaseAction implements Action {
    private MobileDriver<MobileElement> driver;

    protected BaseAction(MobileDriver<MobileElement> driver) {
        this.driver = driver;
    }

    protected MobileDriver<MobileElement> driver() {
        return driver;
    }
}
