package com.qatestlab.base.actions;

import com.qatestlab.base.BaseAction;
import com.qatestlab.utils.Direction;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.interactions.Action;

/**
 * Created by aptashnik on 12/12/2016.
 */
public class SwipeByPercentAction extends BaseAction {
    private static final int SWIPE_TIME = 3000;

    private Direction direction;
    private int startXPercent, startYPercent, offsetPercent;
    private Integer tmpOffset;

    public SwipeByPercentAction(MobileDriver<MobileElement> driver, com.qatestlab.utils.Direction direction, int xStartPercent, int yStartPercent, int offsetPercent) {
        super(driver);
        this.direction = direction;
        startXPercent = xStartPercent;
        startYPercent = yStartPercent;
        this.offsetPercent = offsetPercent;
    }
    public SwipeByPercentAction withOffsetPercent(int offsetPercent) {
        tmpOffset = offsetPercent;
        return this;
    }
    @Override
    public void perform() {
        Dimension screenSize = driver().manage().window().getSize();
        int xStart, yStart, xDest = 0, yDest = 0, offset;

        xStart = (int) (screenSize.getWidth() / (100.0 / startXPercent));
        yStart = (int) (screenSize.getHeight() / (100.0 / startYPercent));

        switch (direction) {
            case LEFT:
            case RIGHT:
                yDest = yStart;
                offset = (int) (screenSize.getWidth() / (100.0 / (tmpOffset == null ? offsetPercent : tmpOffset)));
                xDest = xStart + (direction == com.qatestlab.utils.Direction.RIGHT ? offset : -offset);
                break;
            case DOWN:
            case UP:
                xDest = xStart;
                offset = (int) (screenSize.getHeight() / (100.0 / (tmpOffset == null ? offsetPercent : tmpOffset)));
                yDest = yStart + (direction == com.qatestlab.utils.Direction.DOWN ? offset : -offset);
                break;
        }
        int finalXDest = xDest;
        int finalYDest = yDest;

        long before = System.currentTimeMillis();
        driver().swipe(xStart, yStart, finalXDest, finalYDest, SWIPE_TIME);
        long swipeDelay = System.currentTimeMillis() - before;
        tmpOffset = null;
        try {
            if(SWIPE_TIME - swipeDelay != 0)
                Thread.sleep(SWIPE_TIME - swipeDelay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
