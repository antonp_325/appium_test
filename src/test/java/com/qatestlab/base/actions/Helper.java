package com.qatestlab.base.actions;

import com.qatestlab.base.BaseTest;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.Dimension;

/**
 * Created by aptashnik on 12/13/2016.
 */
public class Helper {
    private static Dimension screenSize;

    static {
        MobileDriver<MobileElement> driver = BaseTest.driver();
        screenSize = driver.manage().window().getSize();
    }

    public static short widthPercentOfScreen(int arg) {
        return (short) (((double)arg / screenSize.getWidth()) * 100);
    }
    public static short heightPercentOfScreen(int arg) {
        return (short) (((double)arg / screenSize.getHeight()) * 100);
    }
}
