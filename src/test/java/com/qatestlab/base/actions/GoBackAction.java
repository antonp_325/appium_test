package com.qatestlab.base.actions;

import com.qatestlab.base.BaseAction;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;

/**
 * Created by aptashnik on 12/13/2016.
 */
public class GoBackAction extends BaseAction {
    public GoBackAction(MobileDriver<MobileElement> driver) {
        super(driver);
    }

    @Override
    public void perform() {
        driver().navigate().back();
    }
}
