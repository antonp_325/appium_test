package com.qatestlab.base;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Created by aptashnik on 12/21/2016.
 */
public class BasePageMethodInterceptor implements MethodInterceptor {

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        if(methodInvocation.getMethod().getAnnotation(NotIntercept.class) != null) {
            BasePage<?> page = (BasePage) methodInvocation.getThis();
            page.isLoaded();
        }
        return null;
    }
}