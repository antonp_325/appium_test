package com.qatestlab.base;

import com.qatestlab.base.actions.Helper;
import com.qatestlab.base.actions.SwipeByPercentAction;
import com.qatestlab.utils.*;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

/**
 * Created by aptashnik on 12/13/2016.
 */
public class ElementsListWrapped<T extends IWrappedMobileElement> implements IWrappedElementsList<T> {
    private static final int SWIPE2LAST_INDEX = Integer.MAX_VALUE;
    private final int MAX_LIST_VERTICAL_OFFSET_PERCENT;
    private final By ELEMENT_ROOT_LOCATOR;
    private final MobileElement LIST;
    private final IMobileElementWrapper<T> ELEMENT_WRAPPER;
    private final BiPredicate<String, MobileElement> IS_ELEMENTS_SAME;
    private final IElementUIDProvider ELEMENT_UID_PROVIDER;
    private List<MobileElement> currentElementsList;
    //private List<String> previousElementsUIDsList;
    private int offset;
    private Integer LIST_SIZE;
    private SwipeByPercentAction swipeUpAction, swipeDownAction;
    private boolean isEndOfList;

    public ElementsListWrapped(MobileDriver<MobileElement> driver, By listLocator, By elementRootLocator, IElementUIDProvider uidProvider,
                               IMobileElementWrapper<T> mobileElementWrapper) {
        ELEMENT_WRAPPER = mobileElementWrapper;
        ELEMENT_ROOT_LOCATOR = elementRootLocator;
        ELEMENT_UID_PROVIDER = uidProvider;
        IS_ELEMENTS_SAME = (arg1, arg2) -> arg1.equals(ELEMENT_UID_PROVIDER.apply(arg2));
        LIST = driver.findElement(listLocator);
        int yStartPerc = Helper.heightPercentOfScreen(LIST.getCoordinates().onPage().getY() + LIST.getSize().getHeight()) - 1;
        MAX_LIST_VERTICAL_OFFSET_PERCENT =
                Helper.heightPercentOfScreen(LIST.getSize().getHeight() + LIST.getLocation().getY());
        swipeUpAction = new SwipeByPercentAction(driver, Direction.UP, 50, yStartPerc, MAX_LIST_VERTICAL_OFFSET_PERCENT);
        yStartPerc = Helper.heightPercentOfScreen(LIST.getCoordinates().onPage().getY());
        swipeDownAction = new SwipeByPercentAction(driver, Direction.DOWN, 50, yStartPerc + 1, MAX_LIST_VERTICAL_OFFSET_PERCENT);
        currentElementsList = LIST.findElements(ELEMENT_ROOT_LOCATOR);
    }

    private boolean updateList(SwipeByPercentAction swipeAction) {
        assert swipeAction != null;
        String firstVisibleElementID, lastVisibleElementID;
        firstVisibleElementID = ELEMENT_UID_PROVIDER.apply(currentElementsList.get(0));
        lastVisibleElementID = ELEMENT_UID_PROVIDER.apply(currentElementsList.get(currentElementsList.size() - 1));

        int prevListSize = currentElementsList.size();
        swipeAction.perform();

        currentElementsList = LIST.findElements(ELEMENT_ROOT_LOCATOR);
//        if (IS_ELEMENTS_SAME.test(firstVisibleElementID, currentElementsList.get(0))) {
//            isEndOfList = true;
//            return false;
//        }
        if (swipeDownAction.equals(swipeAction)) {
            if (offset == 0)
                return false;
            int i;
            for (i = currentElementsList.size() - 1; i >= 0; i--)
                if (IS_ELEMENTS_SAME.test(firstVisibleElementID, currentElementsList.get(i)))
                    break;
            offset -= prevListSize - (i == -1 ? 0 : (currentElementsList.size() - i));
            isEndOfList = false;
        } else if (swipeUpAction.equals(swipeAction)) {
            if (isEndOfList)
                return false;
            int i;
            for (i = 0; i < currentElementsList.size(); i++)
                if (IS_ELEMENTS_SAME.test(lastVisibleElementID, currentElementsList.get(i)))
                    break;
            offset += prevListSize - (i == currentElementsList.size() ? 0 : i + 1);
            if (i == currentElementsList.size() - 1) {
                isEndOfList = true;
                return false;
            }
        }
        return true;
    }

    public T get(int index) {
        if (swipeToIndex(index))
            return ELEMENT_WRAPPER.wrap(currentElementsList.get(index - offset));
        throw new IndexOutOfBoundsException();
    }

    public T get(Predicate<T> predicate) {
        T wrappedElement;
        try {
            for (int i = 0; i < Integer.MAX_VALUE; i++) {
                wrappedElement = get(i);
                if (predicate.test(wrappedElement))
                    return wrappedElement;
            }
        }
        catch (IndexOutOfBoundsException ignored) { }
        return null;
    }

    private void swipeToBegin() {
        swipeToIndex(0);
    }

    private void swipeToEnd() {
        if (isEndOfList)
            return;
        swipeToIndex(SWIPE2LAST_INDEX);
    }

    private boolean swipeToIndex(int index) {
        SwipeByPercentAction swipeAction;
        if (index < offset)
            swipeAction = swipeDownAction;
        else
            swipeAction = swipeUpAction;
        int verticalSwipePercent, currentSwipePercent;
        outerLoop:
        do {
            if (offset <= index && index < offset + currentElementsList.size())
                return true;
            verticalSwipePercent = 0;
            //TODO check it
            for (MobileElement element : currentElementsList)
                verticalSwipePercent += element.getSize().getHeight();
            verticalSwipePercent = Helper.heightPercentOfScreen(verticalSwipePercent);
            do {
                if(verticalSwipePercent >= MAX_LIST_VERTICAL_OFFSET_PERCENT) {
                    currentSwipePercent = MAX_LIST_VERTICAL_OFFSET_PERCENT;
                    verticalSwipePercent -= MAX_LIST_VERTICAL_OFFSET_PERCENT;
                }
                else {
                    currentSwipePercent = verticalSwipePercent;
                    verticalSwipePercent = 0;
                }
                swipeAction.withOffsetPercent(currentSwipePercent);
                if (!updateList(swipeAction))
                    break outerLoop;
            } while (verticalSwipePercent != 0);
        } while (true);
        return index == SWIPE2LAST_INDEX;
    }

    @Override
    public boolean contains(Predicate<T> predicate) {
        try {
            for (int i = 0; i < Integer.MAX_VALUE; i++)
                if (predicate.test(get(i)))
                    return true;
        }
        catch (IndexOutOfBoundsException ignored) { }
        return false;
    }

    @Override
    public int howManyContains(Predicate<T> predicate) {
        int res = 0;
        for (int i = 0; i < size(); i++)
            res += predicate.test(get(i)) ? 1 : 0;
        return res;
    }

    @Override
    public boolean isEmpty() {
        return currentElementsList.isEmpty();
    }

    @Override
    public int size() {
        if (LIST_SIZE == null) {
            swipeToEnd();
            LIST_SIZE = offset + currentElementsList.size();
        }
        return LIST_SIZE;
    }
}